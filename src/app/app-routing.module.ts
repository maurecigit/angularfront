import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClienteComponent } from './cliente/cliente.component';
import { GerenteComponent } from './gerente/gerente.component';
import { VisualizadorComponent } from './visualizador/visualizador.component';



const routes: Routes = [
  {
    path: 'visualizador',
    component: VisualizadorComponent,
    data: { title: 'Acompanhar Senhas Chamadas' }
  },
  {
    path: 'cliente',
    component: ClienteComponent,
    data: { title: 'Acesso do Cliente' }
  },
  {
    path: 'gerente',
    component: GerenteComponent,
    data: { title: 'Acesso do Gerente' }
  },
  { path: '',
    redirectTo: '/visualizador',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
