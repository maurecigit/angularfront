import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { Senha } from 'src/model/senha';

@Component({
  selector: 'app-gerente',
  templateUrl: './gerente.component.html',
  styleUrls: ['./gerente.component.css']
})

export class GerenteComponent implements OnInit {

  senhaChamada: Senha = { tipo: '', sequencial: '', codigo: '', prioridade: null };
  senhaGerada: Senha = { tipo: '', sequencial: '', codigo: '', prioridade: null };
  isLoadingResults = true;

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService) { }

  ngOnInit() {

    this.getSenha();
  }

  // Retorna última senha chamada pelo atendente
  getSenha() {

    this.api.getSenha()
      .subscribe(data => {

        this.senhaChamada = data;
        console.log(this.senhaChamada);
        this.isLoadingResults = false;

      });
  }

  // Retorna a próxima senha
  chamaProximaSenha(tipo) {

    this.isLoadingResults = true;

    this.api.chamaProximaSenha(tipo)
      .subscribe(res => {

          this.isLoadingResults = false;
          this.senhaGerada = res;
                    
        }, (err) => {

          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

  // Gerente clicou em reiniciar senha - tipo: Normal ou Preferencial
  reiniciaSenha(tipo) {

    this.isLoadingResults = true;

    this.api.reiniciaSenha(tipo)
      .subscribe(res => {

          this.isLoadingResults = false;

        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

}