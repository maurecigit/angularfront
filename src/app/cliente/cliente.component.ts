import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { Senha } from 'src/model/senha';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})

export class ClienteComponent implements OnInit {

  senhaGerada: Senha = { tipo: '', sequencial: '', codigo: '', prioridade: null };
  isLoadingResults = false;
  
  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService) { }

  ngOnInit() { }

  // Cliente clicou em gerar senha - tipo: Normal ou Preferencial
  geraSenha(tipo) {

    this.isLoadingResults = true;

    this.api.geraSenha(tipo)
      .subscribe(res => {

          this.isLoadingResults = false;
          this.senhaGerada = res;

        }, (err) => {

          console.log(err);
          this.isLoadingResults = false;

        }
      );
  }

}
