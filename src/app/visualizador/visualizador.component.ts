import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { Senha } from 'src/model/senha';

@Component({
  selector: 'app-visualizador',
  templateUrl: './visualizador.component.html',
  styleUrls: ['./visualizador.component.css']
})
export class VisualizadorComponent implements OnInit {

  interval;

  senhaChamada: Senha = { tipo: '1', sequencial: '1', codigo: '1', prioridade: null };
  isLoadingResults = true;

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService) { }

  ngOnInit() {

    this.interval = setInterval(() => {this.getSenha();}, 1000);
  
  }

  ngOnDestroy() {

    clearInterval(this.interval); 
  }

  // Retorna última senha chamada pelo atendente
  getSenha() {

    this.api.getSenha()
      .subscribe(data => {

        this.senhaChamada = data;

        if(typeof this.senhaChamada === "undefined"){

          this.senhaChamada ={ tipo: '', sequencial: '', codigo: '0', prioridade: null };        
        }

        this.isLoadingResults = false;        
      });
  }
}
