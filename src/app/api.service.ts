import { Injectable, EventEmitter } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Senha } from 'src/model/senha';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

const httpOptions = {

  headers: new HttpHeaders({'Content-Type': 'application/json'})

};

const apiUrl = 'http://localhost:8090/app-server/senha';


@Injectable({

  providedIn: 'root'

})
export class ApiService {

  constructor(private http: HttpClient) { }

  // Última senha chamada pelo atendente (cliente.ts/visualizador.ts)
  getSenha(): Observable<Senha> {

    return this.http.get<Senha>(apiUrl).pipe(
      tap(_ => console.log(`leu o senha `)),
      catchError(this.handleError<Senha>(`getSenha`))
    );
  }

   // Cliente clicou em gerar senha - tipo: Normal ou Preferencial (cliente.ts)
  geraSenha(tipo): Observable<Senha> {

    const apiUrlGeraSenha = apiUrl + '/' + tipo;

    return this.http.post<Senha>(apiUrlGeraSenha, tipo, httpOptions).pipe(
      tap(_ => console.log(`leu o senha `)),
      catchError(this.handleError<Senha>(`getSenha`))
    );

  }

  //  Gerente clicou em próxima senha (gerente.ts)
  chamaProximaSenha(tipo): Observable<Senha> {

    return this.http.post<Senha>(apiUrl, tipo, httpOptions).pipe(
      tap(_ => console.log(`leu a senha-chamaProximaSenha `)),
      catchError(this.handleError<Senha>(`chamaProximaSenha`))
    );

  }

  // Gerente clicou em reiniciar senha - tipo: Normal ou Preferencial (gerente.ts)
  reiniciaSenha(tipo): Observable<Senha> {

    const apiUrlGeraSenha = apiUrl + '/' + tipo;
    
    return this.http.put<Senha>(apiUrlGeraSenha, tipo, httpOptions).pipe(
      tap(_ => console.log(`leu a senha-reiniciaSenha `)),
      catchError(this.handleError<Senha>(`reiniciaSenha`))
    );

  }

  private handleError<T> (operation = 'operation', result?: T) {

    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }
}
