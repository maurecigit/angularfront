## Pilha de tecnologias

	Este projeto foi feito com Angular 8.3.23: [Angular CLI](https://github.com/angular/angular-cli) version 8.3.23.
	Utilizado node: 12.14.1 (pode ser qualquer versão).
	Este é o front-end em Angular 8.
	Já o back-end é baixado em: https://bitbucket.org/maurecigit/angular/get/master.zip
	O back-end também pode ser baixado com git clone: https://maurecigit@bitbucket.org/maurecigit/angular.git

## Baixando o projeto do github

    Página do bitbucket: https://bitbucket.org/maurecigit/angularfront/src/master/
	$ mkdir -p /workspace/desafio
	$ cd /workspace/desafio
	$ git clone https://maurecigit@bitbucket.org/maurecigit/angularfront.git

	Ou dowload: https://bitbucket.org/maurecigit/angularfront/get/master.zip

## Para Abrir Como desenvolvedor

Basicamente você deve ter o angular-cli instalado e, na pasta raiz do projeto (normalmente a src) você deve abrir um prompt de comando e rodar o comando npm install que realizará o download de todas as dependências desse projeto.

Para rodar esse comando no prompt, você precisa do nodejs instalado também.

Após esse passos você roda um ng serve para subir o servidor local. 

(fonte: https://pt.stackoverflow.com/questions/269569/como-importar-um-projeto-existente-com-angular-cli).


Rode o comando `ng serve`. 

Navegue até `http://localhost:4200/`.

## Para rodar o Build apenas como usuário

Já pronto para subir, o build jestá na pasta `dist/`. Basta jogar a pastinha `dist/` em qualquer servidor web.

Ex: no wampserve você pode copiar a pasta /dist do projeto angular para a pasta /www do wampserve e acessar localhost/dist e lá estará sua pagina.

(fonte: https://pt.stackoverflow.com/questions/207059/como-publicar-um-projeto-angular-cli-angular-4-no-meu-servidor).

## Manual do usuário

Navegue até `http://localhost:4200/`.

Menu "Painel": é possível ao público acompanhar a chamada das senhas. Exibe a última senha chamada pelo atendente. A senha é atualizada automaticamente sempre que um atendente chama a próxima senha.

Menu "Gerar Senha": é possível ao cliente gerar novas senhas, que podem ser de dois tipos: NORMAL e PREFERENCIAL.

Menu "Chamar Senha": O gerente pode reiniciar as senhas. Também chamar a próxima senha. O sistema garante que as senhas do tipo preferencial sejam chamada antes das de tipo normal.